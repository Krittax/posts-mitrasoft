import Image from "react-bootstrap/Image";

function AuthorInfo() {
	return (
		<div className="d-flex flex-column">
			<div className="mb-2" style={{ width: "30%", height: "auto" }}>
				<Image
					style={{ width: "100%", height: "100%" }}
					src={process.env.PUBLIC_URL + "/assets/images/my-avatar.svg"}
				></Image>
			</div>
			<div>
				<h3>Сергей Подлесный</h3>
				<p>podlesnyisergeyivanovich@gmail.com</p>
			</div>
		</div>
	);
}

export { AuthorInfo };

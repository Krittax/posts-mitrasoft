import { NoResultNotificationProps } from "./types";

function NoResultNotification({ text }: NoResultNotificationProps) {
	return <h3>{text}</h3>
}

export { NoResultNotification };
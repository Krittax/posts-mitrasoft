export interface NoResultNotificationProps {
	text: string;
}
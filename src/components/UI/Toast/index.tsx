import ToastContainer from "react-bootstrap/Toast";

import { ToastProps } from "./types";

import styles from "./styles.module.scss";

function Toast({ visible, message, onClose }: ToastProps) {
	return (
		<ToastContainer className={styles.toast} show={visible} onClose={onClose}>
			<ToastContainer.Header className="d-flex justify-content-between">
				<strong>Ошибка!</strong>
			</ToastContainer.Header>
			<ToastContainer.Body>
				<strong>{message}</strong>
			</ToastContainer.Body>
		</ToastContainer>
	);
}

export { Toast };

export interface LoadingSpinnerProps {
	visible: boolean;
	size?: "sm";
	classes: string;
}
import Spinner from "react-bootstrap/Spinner";

import { LoadingSpinnerProps } from "./types";

function LoadingSpinner({ visible, size, classes }: LoadingSpinnerProps) {
	if (visible) {
		return <Spinner animation="border" role="status" className={classes} size={size}>
				<span className="visually-hidden">Loading...</span>
			</Spinner>
	}
	return <></>;
}

export { LoadingSpinner };

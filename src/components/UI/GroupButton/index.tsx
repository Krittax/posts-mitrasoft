import { memo } from "react";

import Button from "react-bootstrap/Button";
import ButtonGroup from "react-bootstrap/ButtonGroup";

import { GroupButtonProps } from "./types";

import styles from "./styles.module.scss";

const GroupButton = memo(({ configs }: GroupButtonProps) => {
	return (
		<ButtonGroup aria-label="Basic example" className={styles.sortingNavWrap}>
			{configs.map((config, index) => (
				<Button
					key={index}
					active={config.active}
					disabled={config.disabled}
					variant="secondary"
					onClick={config.onClick}
				>
					{config.text}
				</Button>
			))}
		</ButtonGroup>
	);
});

export { GroupButton };

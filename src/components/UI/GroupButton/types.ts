export interface GroupButtonProps {
	configs: {
		active: boolean;
		disabled: boolean;
		text: string;
		onClick: () => void;
	}[]
}
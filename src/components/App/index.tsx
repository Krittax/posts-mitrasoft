import { Outlet, Link } from "react-router-dom";

import Container from "react-bootstrap/Container";
import Nav from "react-bootstrap/Nav";
import Navbar from "react-bootstrap/Navbar";
import Offcanvas from "react-bootstrap/Offcanvas";

import { AuthorInfo } from "components/common/AuthorInfo";

import HEADER_NAV from "data/header-nav.json";

import styles from "./styles.module.scss";

function App() {
	return (
		<div className="wrapper">
			<header className={styles.header}>
				<Navbar bg="light" expand={false} fixed="top">
					<Container>
						<Navbar.Brand as={Link} to="/">
							Posts-Mitrasoft
						</Navbar.Brand>
						<Navbar.Toggle aria-controls="basic-navbar-nav" />
						<Navbar.Offcanvas
							id={`offcanvasNavbar-expand`}
							aria-labelledby={`offcanvasNavbarLabel-expand`}
							placement="start"
						>
							<Offcanvas.Header closeButton>
								<AuthorInfo />
							</Offcanvas.Header>
							<Offcanvas.Body>
								<Nav className="justify-content-end flex-grow-1 pe-3">
									{HEADER_NAV.map((nav, index) => (
										<Nav.Link key={index} as={Link} to={nav.path}>
											{nav.text}
										</Nav.Link>
									))}
								</Nav>
							</Offcanvas.Body>
						</Navbar.Offcanvas>
					</Container>
				</Navbar>
			</header>
			<main>
				<Outlet />
			</main>
		</div>
	);
}

export { App };

import { FetchError } from "api";

export const isFetchErrorTypeGuard = (data: FetchError | any): data is FetchError => data instanceof Object && "message" in data;
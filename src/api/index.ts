export type { Post, Comment, User, FetchError } from "api/types";

export { getAllPosts, getPostComments } from "api/services/posts";
export { getUserDetails, getUserPosts } from "api/services/users";

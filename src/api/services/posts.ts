import axios from "axios";

import { GET_ALL_POSTS_URL, GET_POST_COMMENTS_URL, DUMMY_DELAY } from "api/constants";

import { delay } from "utils/api";

import { Post, Comment, FetchError } from "api/types";

export async function getAllPosts(): Promise<Post[] | FetchError> {
	try {
		const { data } = await axios.get<Post[]>( GET_ALL_POSTS_URL() );

		return delay(() => data, DUMMY_DELAY);
	}
	catch(err) {
		return delay(() => ({ message: "Ошибка загрузки постов" }), DUMMY_DELAY);
	}
}

export async function getPostComments(postId: number): Promise<Comment[] | FetchError> {
	try {
		const { data } = await axios.get<Comment[]>( GET_POST_COMMENTS_URL(postId) );

		return delay(() => data, DUMMY_DELAY);
	}
	catch(err) {
		return delay(() => ({ message: "Ошибка загрузки комментариев" }), DUMMY_DELAY);
	}
}

import axios from "axios";

import { GET_USER_POSTS_URL, GET_USER_DETAILS_URL, DUMMY_DELAY } from "api/constants";

import { delay } from "utils/api";

import { Post, User, FetchError } from "api/types";

export async function getUserDetails(userId: number): Promise<User | FetchError> {
	try {
		const { data } = await axios.get<User>( GET_USER_DETAILS_URL(userId) );

		return delay(() => data, DUMMY_DELAY);
	}
	catch(err) {
		return delay(() => ({ message: "Ошибка загрузки данных пользователя" }), DUMMY_DELAY);
	}
}

export async function getUserPosts(userId: number): Promise<Post[] | FetchError> {
	try {
		const { data } = await axios.get<Post[]>( GET_USER_POSTS_URL(userId) );

		return delay(() => data, DUMMY_DELAY);
	}
	catch(err) {
		return delay(() => ({ message: "Ошибка загрузки постов пользователя" }), DUMMY_DELAY);
	}
}
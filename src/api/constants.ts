export const DUMMY_DELAY = 500;

const BASE_JSONPLACEHOLDER_URL = "https://jsonplaceholder.typicode.com";

export const GET_ALL_POSTS_URL = () => BASE_JSONPLACEHOLDER_URL + "/posts";
export const GET_POST_COMMENTS_URL = (postId: number) => BASE_JSONPLACEHOLDER_URL + "/posts/" + postId + "/comments";

export const GET_USER_DETAILS_URL = (userId: number) => BASE_JSONPLACEHOLDER_URL + "/users/" + userId;
export const GET_USER_POSTS_URL = (userId: number) => GET_USER_DETAILS_URL(userId) + "/posts";

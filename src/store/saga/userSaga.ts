import { put, takeEvery, call, spawn, delay } from "redux-saga/effects";

import { getUserPosts, getUserDetails, getPostComments } from "api";

import { userActions, ActionKindUser } from "store/slices/user/actions";

import { TOAST_TIMEOUT } from "data/constants";

import { isFetchErrorTypeGuard } from "utils/predicates";

import { PayloadAction } from "@reduxjs/toolkit/dist/createAction";
import { Post, User, Comment, FetchError } from "api";


function* loadUserDetails(userId: number) {
	yield put( userActions.setUserDetailsLoading({ isLoading: true }) );

	const data: User | FetchError = yield call(getUserDetails, userId);

	if ( isFetchErrorTypeGuard(data) ) {
		yield put( userActions.setError({ message: data.message }) );
		yield delay(TOAST_TIMEOUT);
		yield put( userActions.setError({ message: "" }) );
	}
	else {
		yield put( userActions.setUserDetails({ userDetails: data }) );
		yield put( userActions.setUserDetailsLoading({ isLoading: false }) );
	}
}

function* loadUserPosts(userId: number) {
	yield put( userActions.setPostsLoading({ isLoading: true }) );

	const data: Post[] | FetchError = yield call(getUserPosts, userId);

	if ( isFetchErrorTypeGuard(data) ) {
		yield put( userActions.setError({ message: data.message }) );
		yield delay(TOAST_TIMEOUT);
		yield put( userActions.setError({ message: "" }) );
	}
	else {
		yield put( userActions.setPosts({ posts: data }) );
		yield put( userActions.setPostsLoading({ isLoading: false }) );
	}
}

function* fetchUserDetailsWorker(action: PayloadAction<{ userId: number }>) {
	const userId = action.payload.userId;

	yield spawn(loadUserDetails, userId);
	yield spawn(loadUserPosts, userId);
}

function* fetchUserPostCommentsWorker(action: PayloadAction<{ postId: number }>) {
	const postId = action.payload.postId;

	yield put( userActions.setCommentsLoading({ postId, isLoading: true }) );

	const data: Comment[] | FetchError = yield call(getPostComments, postId);

	if ( isFetchErrorTypeGuard(data) ) {
		yield put( userActions.setError({ message: data.message }) );
		yield delay(TOAST_TIMEOUT);
		yield put( userActions.setError({ message: "" }) );
	}
	else {
		yield put( userActions.setComments({ postId, comments: data }) );
		yield put( userActions.setCommentsLoading({ postId, isLoading: false }) );
	}
}

export function* userWatcher() {
	yield takeEvery(ActionKindUser.fetchUserDetails, fetchUserDetailsWorker);
	yield takeEvery(ActionKindUser.fetchUserPostComments, fetchUserPostCommentsWorker);
}
import { put, takeEvery, call, delay } from "redux-saga/effects";

import { getAllPosts, getPostComments } from "api";

import { postsActions, ActionKindPosts } from "store/slices/posts/actions";

import { TOAST_TIMEOUT } from "data/constants";

import { isFetchErrorTypeGuard } from "utils/predicates";

import { PayloadAction } from "@reduxjs/toolkit/dist/createAction";
import { Post, Comment, FetchError } from "api";

function* fetchPostsWorker() {
	yield put( postsActions.setPostsLoading({ isLoading: true }) );

	const data: Post[] | FetchError = yield call(getAllPosts);

	if ( isFetchErrorTypeGuard(data) ) {
		yield put( postsActions.setError({ message: data.message }) );
		yield delay(TOAST_TIMEOUT);
		yield put( postsActions.setError({ message: "" }) );
	}
	else {
		yield put( postsActions.setPosts({ posts: data }) );
		yield put( postsActions.setPostsLoading({ isLoading: false }) );
	}
}

function* fetchPostCommentsWorker(action: PayloadAction<{ postId: number }>) {
	const postId = action.payload.postId;

	yield put( postsActions.setCommentsLoading({ postId, isLoading: true }) );

	const data: Comment[] | FetchError = yield call(getPostComments, postId);

	if ( isFetchErrorTypeGuard(data) ) {
		yield put( postsActions.setError({ message: data.message }) );
		yield delay(TOAST_TIMEOUT);
		yield put( postsActions.setError({ message: "" }) );
	}
	else {
		yield put( postsActions.setComments({ postId, comments: data }) );
		yield put( postsActions.setCommentsLoading({ postId, isLoading: false }) );
	}
}

export function* postsWatcher() {
	yield takeEvery(ActionKindPosts.fetchPosts, fetchPostsWorker);
	yield takeEvery(ActionKindPosts.fetchPostComments, fetchPostCommentsWorker);
}
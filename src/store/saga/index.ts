import { all, spawn } from "redux-saga/effects";

import { postsWatcher } from "./postsSaga";
import { userWatcher } from "./userSaga";

export function* rootWatcher () {
	yield all([ 
		spawn(postsWatcher),
		spawn(userWatcher)
	]);
}
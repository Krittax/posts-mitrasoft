import { createSlice } from "@reduxjs/toolkit";

import { PayloadAction } from "@reduxjs/toolkit/dist/createAction";

import {
	PostsStore,
	SetCommentsPayload,
	ToggleVisibilityCommentsPayload,
	SetPostsLoadingPayload,
	SetCommentsLoadingPayload,
	SetPostsPayload,
	SetPaginationPagePayload,
	SetSearchValuePayload,
	SetErrorPayload
} from "store/slices/posts/types";

const initialPostsState: PostsStore = {
	error: "",
	searchValue: "",
	paginationPage: 1,
	loading: false,
	posts: []
};

const postsSlice = createSlice({
	name: "posts",
	initialState: initialPostsState,
	reducers: {
		setPostsLoading(state, action: PayloadAction<SetPostsLoadingPayload>) {
			state.loading = action.payload.isLoading;
		},
		setPosts(state, action: PayloadAction<SetPostsPayload>) {
			const convertedPosts = action.payload.posts.map( post => ({
				...post,
				loadingComments: false,
				commentsVisible: false,
				comments: [],
			}) );

			state.posts = convertedPosts;
		},
		setCommentsLoading(state, action: PayloadAction<SetCommentsLoadingPayload>) {
			const foundPost = state.posts.find( post => post.id === action.payload.postId );

			if (foundPost) {
				foundPost.loadingComments = action.payload.isLoading;
			}
		},
		setComments(state, action: PayloadAction<SetCommentsPayload>) {
			const foundPost = state.posts.find( post => post.id === action.payload.postId );

			if (foundPost) {
				foundPost.comments = action.payload.comments;
			}
		},
		toggleVisibilityComments(state, action: PayloadAction<ToggleVisibilityCommentsPayload>) {
			const foundPost = state.posts.find( post => post.id === action.payload.postId );

			if (foundPost) {
				foundPost.commentsVisible = !foundPost.commentsVisible;
			}
		},
		setPaginationPage(state, action: PayloadAction<SetPaginationPagePayload>) {
			const page = action.payload.page;

			state.paginationPage = page;
		},
		setSearchValue(state, action: PayloadAction<SetSearchValuePayload>) {
			state.searchValue = action.payload.value;
			state.paginationPage = 1;
		},
		setError(state, action: PayloadAction<SetErrorPayload>) {
			state.error = action.payload.message;
		}
	}
});

const postsReducer = postsSlice.reducer;

export { postsSlice, postsReducer };
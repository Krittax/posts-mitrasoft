import { postsSlice } from ".";

export enum ActionKindPosts {
	fetchPosts = "FETCH_POSTS",
	fetchPostComments = "FETCH_POST_COMMENTS",
}

export const postsActions = postsSlice.actions;

export const fetchPostsAction = () => ({ type: ActionKindPosts.fetchPosts });
export const fetchPostCommentsAction = (postId: number) => ({
	type: ActionKindPosts.fetchPostComments,
	payload: { postId },
});

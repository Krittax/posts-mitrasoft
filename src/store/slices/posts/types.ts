import { Post, Comment } from "api";

export interface PostsStore {
	error: string;
	paginationPage: number;
	searchValue: string;
	loading: boolean;
	posts: PostState[];
}

export type PostState = Post & {
	commentsVisible: boolean;
	comments: Comment[];
	loadingComments: boolean,
};

export interface SetCommentsPayload {
	postId: number;
	comments: Comment[];
}

export interface ToggleVisibilityCommentsPayload {
	postId: number;
}

export interface SetPostsLoadingPayload {
	isLoading: boolean;
}

export interface SetCommentsLoadingPayload {
	postId: number;
	isLoading: boolean;
}

export interface SetPostsPayload {
	posts: Post[];
}

export interface SetPaginationPagePayload {
	page: number;
}

export interface SetSearchValuePayload {
	value: string;
}

export interface SetErrorPayload {
	message: string;
}
import { PostState } from "store/slices/posts/types";
import { User, Post, Comment } from "api";

export interface UserStore {
	error: string;
	loadingPosts: boolean;
	loadingUserDetails: boolean
	userDetails: User | null;
	posts: PostState[];
}

export interface SetLoadingPayload {
	isLoading: boolean;
}

export interface SetPostsPayload {
	posts: Post[];
}

export interface SetCommentsLoadingPayload {
	postId: number;
	isLoading: boolean;
}

export interface SetCommentsPayload {
	postId: number;
	comments: Comment[];
}

export interface ToggleVisibilityCommentsPayload {
	postId: number;
}

export interface SetUserDetailsPayload {
	userDetails: User;
}

export interface SetErrorPayload {
	message: string;
}
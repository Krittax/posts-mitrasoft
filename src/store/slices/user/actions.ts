import { userSlice } from ".";

export enum ActionKindUser {
	fetchUserDetails = "FETCH_USER_DETAILS",
	fetchUserPostComments = "FETCH_USER_POST_COMMENTS"
}

export const userActions = userSlice.actions;

export const fetchUserDetailsAction = (userId: number) => ({
	type: ActionKindUser.fetchUserDetails,
	payload: { userId },
});

export const fetchUserPostCommentsAction = (postId: number) => ({
	type: ActionKindUser.fetchUserPostComments,
	payload: { postId },
});
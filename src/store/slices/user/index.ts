import { createSlice } from "@reduxjs/toolkit";

import { PayloadAction } from "@reduxjs/toolkit/dist/createAction";

import {
	UserStore,
	SetLoadingPayload,
	SetCommentsPayload,
	ToggleVisibilityCommentsPayload,
	SetCommentsLoadingPayload,
	SetPostsPayload,
	SetUserDetailsPayload,
	SetErrorPayload
} from "store/slices/user/types";

const initialUserState: UserStore = {
	error: "",
	loadingPosts: false,
	loadingUserDetails: false,
	userDetails: null,
	posts: []
};

const userSlice = createSlice({
	name: "user",
	initialState: initialUserState,
	reducers: {
		setUserDetailsLoading(state, action: PayloadAction<SetLoadingPayload>) {
			state.loadingUserDetails = action.payload.isLoading;
		},
		setUserDetails(state, action: PayloadAction<SetUserDetailsPayload>) {
			state.userDetails = action.payload.userDetails;
		},
		setPostsLoading(state, action: PayloadAction<SetLoadingPayload>) {
			state.loadingPosts = action.payload.isLoading;
		},
		setPosts(state, action: PayloadAction<SetPostsPayload>) {
			const convertedPosts = action.payload.posts.map( post => ({
				...post,
				loadingComments: false,
				commentsVisible: false,
				comments: [],
			}) );

			state.posts = convertedPosts;
		},
		setCommentsLoading(state, action: PayloadAction<SetCommentsLoadingPayload>) {
			const foundPost = state.posts.find( post => post.id === action.payload.postId );

			if (foundPost) {
				foundPost.loadingComments = action.payload.isLoading;
			}
		},
		setComments(state, action: PayloadAction<SetCommentsPayload>) {
			const foundPost = state.posts.find( post => post.id === action.payload.postId );

			if (foundPost) {
				foundPost.comments = action.payload.comments;
			}
		},
		toggleVisibilityComments(state, action: PayloadAction<ToggleVisibilityCommentsPayload>) {
			const foundPost = state.posts.find( post => post.id === action.payload.postId );

			if (foundPost) {
				foundPost.commentsVisible = !foundPost.commentsVisible;
			}
		},
		resetState(state) {
			state.posts = [];
			state.userDetails = null;
		},
		setError(state, action: PayloadAction<SetErrorPayload>) {
			state.error = action.payload.message;
		}
	}
});

const userReducer = userSlice.reducer;

export { userSlice, userReducer };
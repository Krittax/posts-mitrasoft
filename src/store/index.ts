import { configureStore } from "@reduxjs/toolkit";
import createSagaMiddleware from "redux-saga";

import { postsReducer } from "./slices/posts";
import { userReducer } from "./slices/user";

import { rootWatcher } from "./saga";

const sagaMiddleware = createSagaMiddleware();

export const store = configureStore({
	reducer: {
		posts: postsReducer,
		user: userReducer
	},
	middleware: (getDefaultMiddleware) => getDefaultMiddleware().concat(sagaMiddleware)
});

sagaMiddleware.run(rootWatcher);

export type RootState = ReturnType<typeof store.getState>
import { useSelector } from "react-redux";
import { Link } from "react-router-dom";
import cn from "classnames";

import Button from "react-bootstrap/Button";

import styles from "./styles.module.scss";

import { RootState } from "store";

function UserNav() {
	const userDetailsLoading = useSelector(
		(state: RootState) => state.user.loadingUserDetails
	);

	return (
		<div className={cn("d-flex justify-content-start mb-3", styles.navWrap)}>
			<Link to="..">
				<Button variant="secondary" disabled={userDetailsLoading}>
					Назад
				</Button>
			</Link>
		</div>
	);
}

export { UserNav };

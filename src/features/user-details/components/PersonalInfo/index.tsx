import { useSelector } from "react-redux";
import cn from "classnames";

import Card from "react-bootstrap/Card";

import { LoadingSpinner } from "components/UI/LoadingSpinner";

import { USER_DETAILS_LABELS } from "data/constants";

import { RootState } from "store";

import styles from "./styles.module.scss";

function PersonalInfo() {
	const userDetails = useSelector((state: RootState) => state.user.userDetails);
	const userDetailsLoading = useSelector(
		(state: RootState) => state.user.loadingUserDetails
	);

	const convertedUserPersonalInfo: {
		id: string;
		value: string;
		label: string;
	}[] = [];

	if (userDetails) {
		const pickedUserPersonalInfo: Record<string, string> = {
			name: userDetails.name,
			username: userDetails.username,
			email: userDetails.email,
			phone: userDetails.phone,
			website: userDetails.website,
			street: userDetails.address.street,
			suite: userDetails.address.suite,
			city: userDetails.address.city,
			zipcode: userDetails.address.zipcode,
			companyName: userDetails.company.name,
		};

		for (const prop in pickedUserPersonalInfo) {
			const value = pickedUserPersonalInfo[prop];

			if (typeof value === "string") {
				convertedUserPersonalInfo.push({
					id: value,
					value,
					label: USER_DETAILS_LABELS[prop],
				});
			}
		}
	}

	return (
		<div className={cn("mb-3", styles.personalInfoWrap)}>
			<div className="d-flex justify-content-start">
				<h4>
					Детали о {userDetails ? userDetails.username + ":" : "пользователе:"}
				</h4>
			</div>
			<LoadingSpinner visible={userDetailsLoading} classes="my-3" />
			{!!convertedUserPersonalInfo.length && (
				<Card>
					<Card.Header as="h5">Персональная информация</Card.Header>
					<Card.Body>
						{convertedUserPersonalInfo.map((personalInfo) => (
							<div
								key={personalInfo.id}
								className="d-flex justify-content-between"
							>
								<Card.Text>{personalInfo.label}</Card.Text>
								<Card.Text>{personalInfo.value}</Card.Text>
							</div>
						))}
					</Card.Body>
				</Card>
			)}
		</div>
	);
}

export { PersonalInfo };

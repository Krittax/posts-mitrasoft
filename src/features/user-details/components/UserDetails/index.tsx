import { useEffect } from "react";

import { useDispatch, useSelector } from "react-redux";
import { useParams } from "react-router-dom";

import { PersonalInfo } from "../PersonalInfo";
import { PersonalPosts } from "../PersonalPosts";

import { NoResultNotification } from "components/UI/NoResultNotification";
import { Toast } from "components/UI/Toast";

import { userActions, fetchUserDetailsAction } from "store/slices/user/actions";

import { RootState } from "store";

let isInit = true;

function UserDetails() {
	const dispatch = useDispatch();
	const { userId } = useParams();

	const userPostsLoading = useSelector(
		(state: RootState) => state.user.loadingPosts
	);
	const userDetailsLoading = useSelector(
		(state: RootState) => state.user.loadingUserDetails
	);
	const userDetails = useSelector((state: RootState) => state.user.userDetails);
	const errorMessage = useSelector((state: RootState) => state.user.error);

	useEffect(() => {
		isInit = false;
	}, []);

	useEffect(() => {
		if (!userId) return;

		dispatch(fetchUserDetailsAction(+userId));

		return () => {
			dispatch(userActions.resetState());
		};
	}, [userId, dispatch]);

	const closeToastHandler = () => {
		dispatch(userActions.setError({ message: "" }));
	}

	const isUserNotFound =
		!userDetails && !userPostsLoading && !userDetailsLoading && !isInit;

	return (
		<>
			<Toast visible={errorMessage !== ""} message={errorMessage} onClose={closeToastHandler} />
			{isUserNotFound && (
				<NoResultNotification text="Пользователь не найден!" />
			)}
			{!isUserNotFound && <>
				<PersonalInfo />
				<PersonalPosts />
			</>}
		</>
	);
}

export { UserDetails };

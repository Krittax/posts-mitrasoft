import { useSelector, useDispatch } from "react-redux";

import { PostList } from "features/posts";

import { LoadingSpinner } from "components/UI/LoadingSpinner";

import { userActions, fetchUserPostCommentsAction } from "store/slices/user/actions";

import { RootState } from "store";

import styles from "./styles.module.scss";

function PersonalPosts() {
	const dispatch = useDispatch();
	const userPostsLoading = useSelector(
		(state: RootState) => state.user.loadingPosts
	);
	const userPosts = useSelector((state: RootState) => state.user.posts);

	const loadPostCommentsHandler = (postId: number) => {
		const foundPost = userPosts.find((post) => post.id === postId);

		if (!foundPost) return;

		dispatch(userActions.toggleVisibilityComments({ postId }));

		if (!foundPost.commentsVisible) {
			dispatch(fetchUserPostCommentsAction(postId));
		}
	};

	return (
		<div className={styles.personalPostsWrap}>
			<div className="d-flex justify-content-start">
				<h4>Посты:</h4>
			</div>
			<LoadingSpinner visible={userPostsLoading} classes="my-3" />
			<PostList
				posts={userPosts}
				onGetPath={() => "."}
				onLoadPostComments={loadPostCommentsHandler}
			/>
		</div>
	);
}

export { PersonalPosts };
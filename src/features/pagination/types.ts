export interface PaginationProps {
	currentPage: number;
	itemsLength: number;
	itemsPerPage: number;
	onChangePage: (page: number) => void;
}
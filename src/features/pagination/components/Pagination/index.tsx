import { memo } from "react";

import BootstrapPagination from "react-bootstrap/Pagination";

import { PaginationProps } from "features/pagination/types";

const Pagination = memo(
	({
		itemsLength,
		currentPage,
		itemsPerPage,
		onChangePage,
	}: PaginationProps) => {
		const totalPagesCount = Math.ceil(itemsLength / itemsPerPage) || 1;

		const pages = [];
		const ellipsisPages = [];
		const startEllipsis = currentPage - 3 < 3 ? 1 : currentPage - 3;
		const endEllipsis = currentPage + 2 > totalPagesCount - 3 ? totalPagesCount : currentPage + 2;

		for (let i = 1; i <= totalPagesCount - 1; i++) {
			pages.push(
				<BootstrapPagination.Item
					key={"pagination-page-" + i}
					active={currentPage === i}
					onClick={() => onChangePage(i)}
				>
					{i}
				</BootstrapPagination.Item>
			);
		}

		ellipsisPages.push(
			<BootstrapPagination.Item
				key={"pagination-page-" + 1}
				active={currentPage === 1}
				onClick={() => onChangePage(1)}
			>
				{1}
			</BootstrapPagination.Item>
		);
		
		if (startEllipsis !== 1 && pages.length >= 9) {
			ellipsisPages.push(<BootstrapPagination.Ellipsis key="start-ellipsis" />);
		}

		let slicedPages = pages.slice(startEllipsis, endEllipsis);
		
		if (endEllipsis === totalPagesCount) {
			slicedPages = pages.slice(totalPagesCount - 7 === 0 ? 1 : totalPagesCount - 7);
		}
		if (startEllipsis === 1) {
			slicedPages = pages.slice(1, 7);
		}
		if (totalPagesCount === 9) {
			slicedPages = pages.slice(1, 8);
		}

		ellipsisPages.push(...slicedPages);
		
		if (endEllipsis !== totalPagesCount && pages.length >= 9) {
			ellipsisPages.push(<BootstrapPagination.Ellipsis key="end-ellipsis" />);
		}

		if (totalPagesCount !== 1) {
			ellipsisPages.push(
				<BootstrapPagination.Item
					key={"pagination-page-" + totalPagesCount}
					active={currentPage === totalPagesCount}
					onClick={() => onChangePage(totalPagesCount)}
				>
					{totalPagesCount}
				</BootstrapPagination.Item>
			);
		}

		return (
			<BootstrapPagination>
				{ellipsisPages}
			</BootstrapPagination>
		);
	}
);

export { Pagination };

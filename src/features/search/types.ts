export interface SearchProps {
	value: string;
	isDisabled: boolean;
	onChangeValue: (value: string) => void;
}
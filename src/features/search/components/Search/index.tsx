import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";

import { SearchProps } from "features/search/types";

function Search({ value, isDisabled, onChangeValue }: SearchProps) {
	const changeValueHandler = (value: string) => {
		onChangeValue(value);
	};

	return (
		<Form className="d-flex">
			<Form.Control
				type="search"
				placeholder="Поиск..."
				className="me-2"
				aria-label="Search"
				disabled={isDisabled}
				value={value}
				onChange={(event) => changeValueHandler(event.target.value)}
			/>
			<Button
				disabled={isDisabled}
				variant="outline-success"
				onClick={() => changeValueHandler("")}
			>
				X
			</Button>
		</Form>
	);
}

export { Search };

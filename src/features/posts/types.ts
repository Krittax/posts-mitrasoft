import { PostState } from "store/slices/posts/types";
import { Comment } from "api";

export interface PostListProps {
	posts: PostState[];
	onLoadPostComments: (postId: number) => void;
	onGetPath: (id: number) => string;
}

export interface PostItemProps {
	postData: PostState;
	onLoadPostComments: (postId: number) => void;
	onGetPath: (id: number) => string;
}

export interface CommentsProps {
	postId: number;
	comments: Comment[];
}
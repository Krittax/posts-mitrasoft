import { useEffect, useCallback, useState, useMemo } from "react";

import { useSelector, useDispatch } from "react-redux";
import cn from "classnames";

import { PostList } from "../PostList";
import { GroupButton } from "components/UI/GroupButton";

import { LoadingSpinner } from "components/UI/LoadingSpinner";
import { NoResultNotification } from "components/UI/NoResultNotification";
import { Toast } from "components/UI/Toast";

import { Search } from "features/search";

import {
	fetchPostsAction,
	fetchPostCommentsAction,
	postsActions,
} from "store/slices/posts/actions";

import { Pagination } from "features/pagination";

import { POSTS_PER_PAGE } from "data/constants";

import { RootState } from "store";

import styles from "./styles.module.scss";

function Posts() {
	const [sortType, setSortType] = useState<"ascending" | "descending" | null>(
		null
	);

	const dispatch = useDispatch();
	const searchValue = useSelector(
		(state: RootState) => state.posts.searchValue
	);
	const paginationPage = useSelector(
		(state: RootState) => state.posts.paginationPage
	);
	const postsLoading = useSelector((state: RootState) => state.posts.loading);
	const posts = useSelector((state: RootState) => state.posts.posts);
	const errorMessage = useSelector((state: RootState) => state.posts.error);

	useEffect(() => {
		dispatch(fetchPostsAction());
	}, [dispatch]);

	const filteredPostsBySearch = useMemo(() => {
		return posts.filter((post) =>
			post.title.toLocaleLowerCase().includes(searchValue.toLocaleLowerCase())
		);
	}, [searchValue, posts]);

	const sortedPosts = useMemo(() => {
		const postsCopy = filteredPostsBySearch.slice();
		if (!sortType) {
			return postsCopy;
		}

		postsCopy.sort((postA, postB) => {
			if (postA.title > postB.title) {
				return 1;
			} else if (postA.title < postB.title) {
				return -1;
			} else {
				return 0;
			}
		});

		if (sortType === "descending") {
			return postsCopy.reverse();
		}

		return postsCopy;
	}, [sortType, filteredPostsBySearch]);

	const postsCount = sortedPosts.length;

	const slicedPostsByPagination = sortedPosts.slice(
		(paginationPage - 1) * POSTS_PER_PAGE,
		paginationPage * POSTS_PER_PAGE
	);

	const loadPostCommentsHandler = (postId: number) => {
		const foundPost = slicedPostsByPagination.find(
			(post) => post.id === postId
		);

		if (!foundPost) return;

		dispatch(postsActions.toggleVisibilityComments({ postId }));

		if (!foundPost.commentsVisible) {
			dispatch(fetchPostCommentsAction(postId));
		}
	};

	const changePostsPaginationPageHandler = useCallback(
		(page: number) => {
			dispatch(postsActions.setPaginationPage({ page }));
		},
		[dispatch]
	);

	const changeSearchValueHandler = (value: string) => {
		dispatch(postsActions.setSearchValue({ value }));
	};

	const closeToastHandler = () => {
		dispatch(postsActions.setError({ message: "" }));
	}

	const changeSortTypeHandler = useCallback(
		(sort: "ascending" | "descending" | null) => {
			if (sort === sortType) {
				setSortType(null);
				return;
			}
			setSortType(sort);
		},
		[sortType]
	);

	const sortingBtnsConfig = useMemo(
		() => [
			{
				active: sortType === "descending",
				disabled: postsLoading,
				text: "По убыванию",
				onClick: () => changeSortTypeHandler("descending"),
			},
			{
				active: sortType === "ascending",
				disabled: postsLoading,
				text: "По возрастанию",
				onClick: () => changeSortTypeHandler("ascending"),
			},
		],
		[sortType, postsLoading, changeSortTypeHandler]
	);

	return (
		<>
			<Toast visible={errorMessage !== ""} message={errorMessage} onClose={closeToastHandler} />
			<div
				className={cn("d-flex flex-column justify-content-between mb-2 flex-md-row", styles.actionWrap)}
			>
				<GroupButton configs={sortingBtnsConfig} />
				<Search
					isDisabled={postsLoading}
					value={searchValue}
					onChangeValue={changeSearchValueHandler}
				/>
			</div>
			<LoadingSpinner visible={postsLoading} classes="my-3" />
			{!postsLoading && !!slicedPostsByPagination.length && (
				<PostList
					posts={slicedPostsByPagination}
					onGetPath={(id) => `user-details/${id}`}
					onLoadPostComments={loadPostCommentsHandler}
				/>
			)}
			{!postsLoading && !slicedPostsByPagination.length && !!searchValue.length && (
				<NoResultNotification text="Ничего не найдено!" />
			)}
			{!postsLoading && !!slicedPostsByPagination.length && (
				<Pagination
					currentPage={paginationPage}
					itemsPerPage={POSTS_PER_PAGE}
					itemsLength={postsCount}
					onChangePage={changePostsPaginationPageHandler}
				/>
			)}
		</>
	);
}

export { Posts };

import ListGroup from "react-bootstrap/ListGroup";

import { CommentsProps } from "features/posts/types";

import styles from "./styles.module.scss";

function Comments({ comments, postId }: CommentsProps) {
	return (
		<ListGroup className="mt-2">
			{comments.map((comment) => (
				<ListGroup.Item key={"post-" + postId + "-comment-" + comment.id}>
					<h6>{comment.email}</h6>
					<p className={styles.commentText}>{comment.body}</p>
				</ListGroup.Item>
			))}
		</ListGroup>
	);
}

export { Comments };

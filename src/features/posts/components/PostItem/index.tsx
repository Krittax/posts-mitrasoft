import { Link } from "react-router-dom";
import cn from "classnames";

import Card from "react-bootstrap/Card";
import Image from "react-bootstrap/Image";
import Button from "react-bootstrap/Button";

import { Comments } from "../Comments";

import { LoadingSpinner } from "components/UI/LoadingSpinner";

import { PostItemProps } from "features/posts/types";

import styles from "./styles.module.scss";

function PostItem({
	postData,
	onLoadPostComments,
	onGetPath,
}: PostItemProps) {
	const {
		id,
		body,
		comments,
		commentsVisible,
		loadingComments,
		title,
		userId,
	} = postData;

	const generatePath = () => {
		return onGetPath(userId);
	};

	return (
		<Card className={cn("mb-2", styles.post)}>
			<Card.Header as="h5">{title}</Card.Header>
			<Card.Body className="d-flex flex-column justify-content-between align-items-center flex-sm-row pb-0">
				<div className={styles.avatarWrap}>
					<Link to={generatePath()}>
						<Image
							src={process.env.PUBLIC_URL + "/assets/images/avatar.svg"}
						></Image>
					</Link>
				</div>
				<div className={styles.postTextWrap}>
					<Card.Text>{body}</Card.Text>
				</div>
			</Card.Body>
			<Card.Body>
				<div className="d-flex justify-content-end align-items-center">
					<LoadingSpinner visible={loadingComments} size="sm" classes="me-2" />
					<Button
						variant="secondary"
						size="sm"
						disabled={loadingComments}
						onClick={() => onLoadPostComments(id)}
					>
						{commentsVisible && !loadingComments
							? "Скрыть комментарии"
							: "Комментарии"}
					</Button>
				</div>
				{commentsVisible && !loadingComments && !!comments.length && (
					<Comments comments={comments} postId={id} />
				)}
			</Card.Body>
		</Card>
	);
}

export { PostItem };

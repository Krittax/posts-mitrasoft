import { PostItem } from "../PostItem";

import { PostListProps } from "features/posts/types";

import styles from "./styles.module.scss";

function PostList({
	posts,
	onLoadPostComments,
	onGetPath,
}: PostListProps) {
	return (
		<div className={styles.postList}>
			{posts.map((post) => (
				<PostItem
					key={"post-" + post.id}
					postData={post}
					onGetPath={onGetPath}
					onLoadPostComments={onLoadPostComments}
				/>
			))}
		</div>
	);
}

export { PostList };

import { createBrowserRouter, Navigate } from "react-router-dom";

import { App } from "components/App";

import { MainPage } from "pages/Main";
import { AboutMePage } from "pages/AboutMe";
import { UserDetailsPage } from "pages/UserDetails";

export const router = createBrowserRouter([
	{
		path: "/",
		element: <App />,
		errorElement: <Navigate to="/" replace />,
		children: [
			{
				index: true,
				element: <MainPage />,
			},
			{
				path: "about-me",
				element: <AboutMePage />,
			},
			{
				path: "user-details/:userId",
				element: <UserDetailsPage />,
			}
		]
	}
]);
export const POSTS_PER_PAGE = 10;

export const TOAST_TIMEOUT = 5000;

export const USER_DETAILS_LABELS: Record<string, string> = {
	name: "Имя:",
	username: "Никнейм:",
	email: "Почта:",
	phone: "Телефон:",
	website: "Сайт:",
	street: "Улица:",
	suite: "Апартаменты:",
	city: "Город:",
	zipcode: "Почтовый индекс:",
	companyName: "Название компании:",
};

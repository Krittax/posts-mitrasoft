import Container from "react-bootstrap/Container";

import { Posts } from "features/posts";

function MainPage() {
	return (
		<section className="py-4">
			<Container className="d-flex flex-column align-items-center">
				<Posts />
			</Container>
		</section>
	);
}

export { MainPage };

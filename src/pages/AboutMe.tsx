import Card from "react-bootstrap/Card";
import Container from "react-bootstrap/Container";

function AboutMePage() {
	return (
		<section className="py-4">
			<Container className="d-flex flex-column align-items-center">
				<div
					className="d-flex flex-column justify-content-start mb-3"
					style={{ maxWidth: "600px", width: "100%" }}
				>
					<Card className="mb-2">
						<Card.Header as="h5">Обо мне</Card.Header>
						<Card.Body>
							<Card.Text>
								Я - ответственный, целеустремленный, трепетно отношусь к
								написанию кода, открыт к новым знаниям и работе в команде. Умею
								быстро осваивать новые технологии и подходы. Очень мотивирован
								на развитие в будущей компании. Имеется опыт коммерческой
								разработки на JavaScript, React, а также TypeScript.
							</Card.Text>
							<Card.Text>
								Владею: JavaScript, React, NextJs, TypeScript, Redux, Redux
								Toolkit, Redux Saga, Git.
							</Card.Text>
							<Card.Text>
								В поиске вакансии на позицию React Developer. Желаемый
								работодатель - продуктовая компания с пониманием моей позиции на
								проекте, перспективами роста, а также дружной и сплоченной
								командой.
							</Card.Text>
						</Card.Body>
					</Card>
					<Card className="mb-2">
						<Card.Header as="h5">Опыт работы</Card.Header>
						<Card.Body>
							<Card.Text>
								Занимался преимущественно e-commerce разработкой b2c и b2b
								проектов. Был опыт решений задач как по четкому ТЗ, так и
								минимально описанных, где необходимо решить как/с помощью чего
								будет достигнут желаемый результат.
							</Card.Text>
							<Card.Text>
								Создавал React-приложения c нуля в связке с JS и TS.
								Проектировал для дальнейшей интеграции API и непосредственно
								используя готовый API. Настраивал SSR. При этом в работе
								применял пути для оптимизации рендера используя современные
								подходы и улучшая качество кода.
							</Card.Text>
							<Card.Text>
								Работал над написанием бизнес-логики разной сложности на чистом
								JavaScript. В работе применял множество сторонних библиотек и
								API (Google Maps, Google Drive, ChartJs, SMTP, Swiper, Metered и
								др.) командой.
							</Card.Text>
						</Card.Body>
					</Card>
					<Card>
						<Card.Header as="h5">Образование (Магистр)</Card.Header>
						<Card.Body>
							<Card.Text>
								Северодонецкий технологический институт Восточноукраинского
								национального университета им. В. Даля, Северодонецк
							</Card.Text>
							<Card.Text>
								Информационные технологии, Компьютерные науки
							</Card.Text>
						</Card.Body>
					</Card>
				</div>
			</Container>
		</section>
	);
}

export { AboutMePage };

import Container from "react-bootstrap/Container";

import { UserNav } from "features/user-details";
import { UserDetails } from "features/user-details";

function UserDetailsPage() {
	return (
		<section className="py-4">
			<Container className="d-flex flex-column align-items-center">
				<UserNav />
				<UserDetails />
			</Container>
		</section>
	);
}

export { UserDetailsPage };
